# Interpretator

Многомодульный проект:
 * interpretator-core - парсер, транслятор, вычислитель 
  * interpretator-web - jetty контейнер для запуска
  
Для запуска в корне проекта:
```
>  sbt
>  project interpretator_web
>  jetty:start
>  browse
```
После этого интерпретатор будет доступен по адресу [http://localhost:8080/](http://localhost:8080/)

Для остановки контейнера: 
```
>  jetty:stop
```
Для автоматического подтягивания обновленных файлов: 
```
> ~;jetty:stop;jetty:start
```

Полезные команды sbt:
```
sbt coverage test
sbt dependencyTree

```


