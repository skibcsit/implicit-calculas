import sbt._
import Keys._

name := "sbt-multi-project-example"
organization in ThisBuild := "edu.birelandef.parser"
scalaVersion in ThisBuild := "2.12.3"


//DEPENDENCIES

lazy val dependencies =
  new {
    val scalatestV      = "3.0.5"
    val combinatorsV    = "1.0.6"
    val scalatraVersion = "2.6.3"
    val logbackVersion = "1.2.3"
    val servletVersion = "3.1.0"

    val combinators           = "org.scala-lang.modules" %%  "scala-parser-combinators"  % combinatorsV
    val scalatest             = "org.scalatest"          %% "scalatest"                  % scalatestV          % "test"
    val scalatra              = "org.scalatra"           %% "scalatra"                   % scalatraVersion
    val scalatra_scalatra     = "org.scalatra"           %% "scalatra-scalate"           % scalatraVersion
    val scalatraTest          = "org.scalatra"           %% "scalatra-scalatest"         % scalatraVersion     % "test"
    val logback               = "ch.qos.logback"         % "logback-classic"             % logbackVersion      % "runtime"
    val servlet_api           = "javax.servlet"          % "javax.servlet-api"           % servletVersion      % "provided"
    val jetty                 = "org.eclipse.jetty"      % "jetty-webapp"                % "9.4.8.v20171121"   % "container"

  }

lazy val commonDependencies = Seq(
  dependencies.scalatest
)

// SETTINGS

lazy val compilerOptions = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)

lazy val commonSettings = Seq(
  sbtPlugin := true,
  scalacOptions ++= compilerOptions,
  resolvers ++= Seq(
    Classpaths.typesafeReleases,
    "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository",
    "Remote Maven Repository" at "https://mvnrepository.com/repos/central",
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )
)

// PROJECTS

lazy val global = project
  .in(file("."))
  .settings(commonSettings)
  .aggregate(
    interpretator_core,
    interpretator_web
  )

lazy val interpretator_core = project
  .in(file("interpretator-core"))
  .settings(
    name := "interpretator-core",
    commonSettings,
    libraryDependencies ++= commonDependencies ++ Seq(
      dependencies.combinators,
      dependencies.scalatest
    )
  )
  .dependsOn()

lazy val interpretator_web = project
  .in(file("interpretator-web"))
  .settings(
    name := "interpretator-web",
    organization := "edu.birelandef.interpretator",
    commonSettings , //++ ScalatraPlugin.scalatraSettings,
    libraryDependencies ++= commonDependencies ++ Seq(
      dependencies.scalatra,
      dependencies.scalatraTest,
      dependencies.logback,
      dependencies.servlet_api,
      dependencies.jetty
    )
  )
      .enablePlugins(SbtTwirl)
      .enablePlugins(ScalatraPlugin)
  .dependsOn(interpretator_core)