function doTranslation() {
    $.ajax({
        type: "POST",
        url: "/translate",
        data: input_editor.getSession().getValue(),
        cache: false,
        contentType:"application/json; charset=utf-8",
        success: function (data) {
            editor.setValue("")
            editor.insert(data);
        }
    });
}