
import org.scalatra._
import javax.servlet.ServletContext

import edu.birelandef.interpretator.MyScalatraServlet

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    context.mount(new MyScalatraServlet, "/*")
  }
}
