package edu.birelandef.interpretator

import java.lang.Exception

import edu.birelandef.interpretator.translator2.Evaluator.eval
import org.scalatra._

class MyScalatraServlet extends ScalatraServlet {


  get("/") {
    views.html.hello()
  }

  post("/translate") {
      val contentLength = request.contentLength.getOrElse(-1).toString.toInt

      val requestBody = request.body

      val response = edu.birelandef.interpretator.translator2.Translator.translate(requestBody)

      if (response.successful) {
        try {
          println("DFGD", response.get.expr)
          val evalResult = edu.birelandef.interpretator.translator2.Evaluator.eval(response.get.expr)
          println("##########################" * 5)
          println("Eval Result")
          println(evalResult)
          evalResult
        } catch {
          case e: Exception => "Error during evaluation"
        }
      }else
        "Error during translation"


  }

}
