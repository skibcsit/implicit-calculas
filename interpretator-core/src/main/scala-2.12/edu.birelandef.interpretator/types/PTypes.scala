package edu.birelandef.interpretator.types

object PTypes {

  sealed trait PType
  case class Var(a: Int)  extends PType
  case class TInt()  extends PType
  case class Fun (in: PContext, out: PContext)  extends PType
  case class Record (name: String, types: List[PType]) extends PType

  sealed trait PContext
  case class Type (tpe: PType) extends PContext
  case class Forall (function: Int => PContext) extends PContext
  case class Rule (in: PContext, out: PContext ) extends PContext


  sealed trait PExp //t e
  case class EVar (e: Int) extends PExp
  case class ELit (e: Int) extends PExp
  case class ELam (c: PContext, f: Int => PExp) extends PExp
  case class EApp (e1: PExp, e2: PExp) extends PExp
  case class ETLam (function: Int => PExp) extends PExp
  case class ETApp (expr: PExp, context: PContext) extends PExp
  case class EQuery (context: PContext) extends PExp
  case class EILam (context: PContext, expr: PExp) extends PExp
  case class EIApp (e1: PExp, e2: PExp) extends PExp
  case class ERecord (name: String, fields: List[(String, PExp)]) extends PExp
  case class EField (name: String)  extends PExp

}
