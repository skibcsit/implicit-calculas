package edu.burelandef.interpretator.types

import edu.birelandef.interpretator.types.PTypes.PExp

object FTypes {

  sealed trait FType
  case class FTVar(a: Int) extends FType
  case class FTInt() extends FType
  case class FTFun (inType: FType, outType: FType ) extends FType
  case class FTForall (function: Int => FType) extends FType
  case class FTRecord (name: String, types: List[FType]) extends FType

  //  data FExp t v =
  //    FEVar v
  //      | FELit Int
  //  | FELam (FType t) (v -> FExp t v)
  //  | FEApp (FExp t v) (FExp t v)
  //  | FETLam (t -> FExp t v)
  //  | FETApp (FExp t v) (FType t)
  //  --   todo ?? что это
  //  | FERecord String [(String, FExp t v)]
  //  | FEField String
  sealed trait FExp
  // исправить на String
  case class FEVar(value: Int) extends FExp
  case class FELit(value: Int) extends FExp
  //todo доопределить
  case class FELam(tpe: FType, function: Int => FExp) extends FExp
  case class FEApp(exp1: FExp, exp2: FExp) extends FExp
  //todo доопределить
  case class FETLam(function: Int => FExp) extends FExp
  case class FETApp(exp: FExp, tpe: FType) extends FExp
  //запись - это именованный список (?) полей
  case class FERecord(name: String, fields:  List[(String, FExp)]) extends FExp
  // указатель на запись
  case class FEField(name: String) extends FExp
}