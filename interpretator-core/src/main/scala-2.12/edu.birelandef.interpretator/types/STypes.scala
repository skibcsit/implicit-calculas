package edu.birelandef.interpretator.types

import edu.birelandef.interpretator.Token.{
  Interface, Scheme}

object STypes {
  /*
  типовая переменная
  Int
  функциональный тип
  интерфейс
   */
  sealed trait SrcType

  case class STVar(v: String) extends SrcType {
    override def toString: String = v
  }

  case class STInt() extends SrcType {
    override def toString: String = "Int"
  }

  case class STFun(t1: SrcType, t2: SrcType) extends SrcType {
    override def toString: String = "(" + t1 + ") -> " + t2
  }

  /**
    * interface Eq α = {eq:α→α→Bool}
    * @param ifaceName Eq
    * @param types α (тут - полиморфный тип)
    */
  case class STIface(ifaceName: String, types: List[SrcType]) extends SrcType {
    override def toString: String = ifaceName + " " + types.map(x => x.toString).mkString(" ")
  }


  /**
    * Содержит
    *   список переменных
    *   дочерние схемы
    *   srctype тип
    */
  trait SrcScheme
  case class Scheme (tvars: List[String], srcShemas: List[Scheme], srctype: SrcType) extends SrcScheme{
    override def toString: String =
      if  (tvars.isEmpty)
        ""
      else "forall " ++ tvars.mkString(", ") + ". " +
        (if (srcShemas.isEmpty)
          ""
        else "{" ++ srcShemas.map(x => x.toString).mkString(",")  ++ "} => "
        ) + srctype
  }

  case class SrcInterface(interface: Interface){
    override def toString: String =
      "interface " + interface.name + " " + interface.types.vars.mkString(" ") + " = { " +
      interface.declarationList.declarations.map(x => (x.varName + ":" + x.tpe)).mkString("; ") +
      " }"
  }


  /**
    * SrcExp:
    *
    * - SEVar
    * - SELit
    * - SELam
    * - SEApp
    * - SELVar
    * - SEField
    * - SELet
    * - SEImplicit
    * - SEQuery
    */
  sealed trait SrcExp
  case class SEVar (str: String) extends SrcExp {
    override def toString: String = str
  }
  case class SELit (e: Int)  extends SrcExp {
    override def toString: String = e.toString
  }
  case class SELam (binder: String, t: Option[SrcType],  body: SrcExp)  extends SrcExp{
    override def toString: String =
      "\\" + binder + (t match {
        case Some(ty) => ": " + ty
        case None => ""
      }) + ". " +  body.toString
  }
  case class SEApp (e1: SrcExp, e2: SrcExp)  extends SrcExp {
    override def toString: String = "(" + e1 + ") " + e2
  }
  case class SELVar (t: Option[SrcType], value: String) extends SrcExp {
    override def toString: String =
      value +  (t match {
        case Some(ty) => ": " + ty
        case None => ""
      })
  }
  case class SEField (t: Option[SrcType], field: String) extends SrcExp {
    override def toString: String = "\"" ++ field ++ "\"" + (t match {
      case Some(ty) => ":" + ty
      case None => ""
    })
  }
  case class SELet (lvar: String, scheme: Scheme, e1: SrcExp, e2:SrcExp)  extends SrcExp{
    override def toString: String = "let " + lvar + ": " + scheme + " = " + e1 + " in " + e2
  }
  case class SEImplicit (lvars: List[String], body: SrcExp)  extends SrcExp{
    override def toString: String = "implicit {" ++ lvars.mkString(", ")  + "} in " + body
  }
  case class SEQuery (t: Option[SrcType])  extends SrcExp{
    override def toString: String = "?[" ++ (t match {
      case Some(ty) => "[" + ty + "]"
      case None => ""
    })
  }
  case class SEAnnot (expr: SrcExp, scheme: SrcScheme)  extends SrcExp
  case class SEImpl (iface: String, defs: List [(String, SrcExp)], t: Option[SrcType])  extends SrcExp{
    override def toString: String =  iface ++ " {" ++
      defs.map( x  => x._1 + " = " + x._2).mkString(", ") ++
      "}" ++ (t match {
      case Some(ty) => " : "
      case None => ""
    })
  }
}
