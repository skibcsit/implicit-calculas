package edu.birelandef.interpretator

import edu.burelandef.interpretator.types.FTypes
import edu.burelandef.interpretator.types.FTypes._
//TODO stub object
object Evaluator {

//  case class EvalEnv (context: Map[Int, FExp])

  def evalAll(expr: FExp): Option[FExp] ={
    eval(Map.empty[Int, FExp],expr, 0)._1
  }

  /**
    * Проверка на значение
    * Являются значениями:
    *   FELit
    *   FELam
    *   FETLam
    *   FERecord
    *   FEField
    * Не являются значениями:
    *   FEVar
    *   FEApp
    *   FETApp
    * @param expr проверяемое выражение
    * @return false, если это апликация или переменная, true - иначе
    */
  def isValue(expr: FTypes.FExp): Boolean = expr match {
    case x: FELit => true
    case x: FELam => true
    case x: FETLam => true
    case x: FERecord => true
    case x: FEField => true
    case _ => false
  }

  def eval (env: Map[Int, FExp], expr: FExp, n: Int): (Option[FExp], Int) =
    expr match {
      //не уверена, что это Int
      case x: FEVar => (env.get(x.value), n)
      case x: FELit => (Some(x), n)
      case x: FELam => (Some(x), n)

      case x: FEApp
        if (isValue(x.exp1) && isValue(x.exp2)) =>
        x.exp1 match {
          case y: FELam => eval(env.+(n -> x.exp2), y.function.apply(n), n + 1)
          case y: FEField => x.exp2 match {
//            case z: FERecord => (z.fields.get(y.name), n)
            case _ => throw new IllegalArgumentException("record lookup should be applied to records")
          }
          case _ => throw new IllegalArgumentException("application should be done with functions")
        }
      case x: FEApp
        if isValue(x.exp1) => val temp = eval( env, x.exp2,  n); eval(env, FEApp(x.exp1,temp._1.get), temp._2)
      case x: FEApp =>  val temp = eval( env, x.exp1,  n); eval(env, FEApp(temp._1.get, x.exp2), temp._2)

      case x: FETLam => (Some(expr), n)

      case x: FETApp
        if isValue(x.exp) =>
          x.exp match {
            case y: FETLam => eval(env, y.function.apply(n) , n + 1)
            case _ => throw new IllegalArgumentException("type application should be done with type abstractions")
          }

      case x: FETApp => val temp = eval(env, x.exp, n); eval(env, FETApp(temp._1.get, x.tpe), temp._2)

      case x: FERecord => (Some(expr), n)
      case x: FEField => (Some(expr), n)
    }

}
