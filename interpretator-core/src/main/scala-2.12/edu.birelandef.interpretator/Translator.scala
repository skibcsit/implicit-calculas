package edu.birelandef.interpretator

import edu.birelandef.interpretator.Token._
import edu.birelandef.interpretator.types.PTypes.{PExp, _}
import edu.birelandef.interpretator.types.STypes
import edu.birelandef.interpretator.types.STypes.{Scheme, SrcType, SEVar => _, _}

/**
  * Транслятор из исходников в систему P
  */
object Translator {

  def translateSrcPgm(program: Program,expr: SrcExp) : PExp = {
    translateSrcExp(program.interfaces.list.flatMap(getTypesOfFields), expr, 0, Map.empty)
  }

  /**
    * Создание мапы имя переменной декларации - схема.
    * Запускается для каждого из интерфейсов.
    * Проходим по всем декларациям(название функции + input/output типы)
    *   создает S тип интерфейса из имени интерфейса и упаковывает все типы в STVar
    *   создает схему (типы интерфейса, дочерних схем - нет, S тип интерфейса )
    * @param interface анализируемый интерфейс (interface Eq α = {eq:α→α→Bool})
    * @return список [название функции-декларации, схема]
    */
  private def getTypesOfFields(interface: Interface) = {
    var result = List[(String, Scheme)]()
    for (decl <- interface.declarationList.declarations) {
      val ifaceType = STIface(interface.name,  interface.types.vars.map(STVar))
      val scheme = Scheme (interface.types.vars, List.empty, ifaceType)
      result = result :+ (decl.varName, scheme)
    }
    result
  }

  def translateSrcExp(env: List[(String, SrcScheme)], expr: SrcExp, n: Int, map: Map[String, Int]): PExp ={
    expr match {
//      case x: SEVar => map.getOrElse(x.value,""); EVar("")
      case x: SELit => ELit(n)
      case x: SELam =>  ELam (Type(translateSrcType(x.t.get, n, map)),(y:Int) => translateSrcExp(env, x.body,n, map.updated(x.binder, y)))
      case x: SEApp => EApp (translateSrcExp(env, x.e1, n, map),translateSrcExp(env, x.e2, n, map))
//      case x: SELVar =>
//      case x: SEField =>
      case x: SELet =>
        val exp1 = mkAllTAbs(x, env, x.scheme.tvars, n, map)
        val env1 = (x.lvar, x.scheme) :: env
        val exp2 = ELam (translateSrcScheme(x.scheme), (y: Int) => translateSrcExp(env1, x.e2, n, map.updated(x.lvar, y)))
        EApp (exp2, exp1)

//      case x: SEImplicit => {
//        val e = translateSrcExp(env, x.body, n, map)
//        val schemes: List[SrcScheme] = x.lvars.map(currentVar => env.toMap.get(currentVar).get)
//        //todo нормально проинициализировать EVar
//        val us: List[EVar] = x.lvars.map ((u: String) => {map.getOrElse(u, n); EVar("")})
//        val iabs = schemes.map(translateSrcScheme).foldLeft(EILam, e)
//        us.foldLeft( EIApp, iabs)
//      }
      case x: STypes.SEQuery => EQuery(translateSrcScheme(lift(x.t.get)))
      case x: SEImpl =>
        ERecord(x.iface, x.defs.map(u => (u._1, translateSrcExp(env, u._2, n, map))))
    }
  }

  def mkAllTAbs (let: SELet, env: List[(String, SrcScheme)],tvars: List[String], n: Int, map: Map[String, Int]): PExp =
    if (tvars.nonEmpty)
      ETLam ((x: Int) => mkAllTAbs(let, env, tvars.tail, n + 1, map.updated(tvars.head, x)))
    else
      let.scheme.srcShemas.map(translateSrcScheme).foldLeft(translateSrcExp(env,let.e1,n ,map)){ (a, b) => EILam(b,a)}

  def unifySrcType(tpe1: SrcType, tpe2: SrcType): List[(STVar, SrcType)] =
    (tpe1, tpe2)  match {
      case (type1: STFun, type2: STFun) => unifySrcType(type1.t1, type2.t1) ++ unifySrcType(type1.t2, type2.t2)
      case (type1: STIface, type2: STIface) if type1.ifaceName==type2.ifaceName =>
        type1.types.zip(type2.types).flatMap(v => unifySrcType(v._1, v._2))
      case (type1: STVar, type2: STVar ) if type1.v == type2.v => List.empty
      case (type1: STVar, type2) => List((type1, type2))
      case (type1: STInt, type2: STInt) => List.empty
      case (type1: STInt, type2: STVar) => List((type2,type1))
//      case (_, _) => throw  Exception
    }

  def applySubstToScheme(subst: List[(String, SrcType)], scheme: Scheme ): Scheme ={
    val subst1: List[(String, SrcType)] = subst.filter(x  => !scheme.tvars.contains(x._1))
    Scheme (scheme.tvars, scheme.srcShemas.map(v => applySubstToScheme(subst1, v)), applySubstToType(subst1, scheme.srctype))
  }

  def applySubstToType(subst: List[(String, SrcType)], tpe: SrcType ): SrcType = tpe match {
    case (tpe: STVar) => subst.toMap.get(tpe.v) match {
      case Some(x) => x
      case _ =>  tpe
    }
    case (tpe: STInt) => STInt()
    case (tpe: STFun) => STFun(applySubstToType(subst,tpe.t1), applySubstToType(subst,tpe.t2))
    case (tpe: STIface) => STIface(tpe.ifaceName, tpe.types.map(v => applySubstToType(subst, v)))
  }



  def translateSrcScheme(scheme: SrcScheme): PContext =
    translateSrcScheme1(scheme, 0, Map.empty)


  def translateSrcScheme1(schema:SrcScheme, n: Int,  env: Map[String, Int]): PContext =
    schema match {
      case x: Scheme if x.tvars.nonEmpty => Forall((y:Int) =>
        translateSrcScheme1(Scheme(x.tvars.tail, x.srcShemas, x.srctype), n, env.updated(x.tvars.head,y)))
      case x: Scheme if x.srcShemas.nonEmpty => Rule(translateSrcScheme1(x.srcShemas.head, n,env),
        translateSrcScheme1(Scheme(List.empty, x.srcShemas.tail, x.srctype), n,env))
      case x: Scheme => Type(translateSrcType(x.srctype ,n, env))
  }

  /*
  Перевод простого типа в схему
   */
  def lift(srctype: SrcType): SrcScheme =  Scheme(List.empty, List.empty, srctype)

  /*
  перевод из SrcType в PType
   */
  def translateSrcType(tpe: SrcType, n: Int, env: Map[String, Int]): PType = tpe match  {
      case x: STInt => TInt()
      case x: STFun => Fun(Type(translateSrcType(x.t1, n, env)), Type(translateSrcType(x.t2, n, env)))
      case x: STIface => Record(x.ifaceName, x.types.map{ty: SrcType => translateSrcType(ty, n, env)})
      case _ => throw new IllegalAccessException
  }

  def transformVarToLVar(expr: SrcExp): SrcExp ={
    transform(Set.empty, expr)
  }


  /**
    * SrcExp:
    *
    * - SEVar
    * - SELit
    * - SELam
    * - SEApp
    * - SELVar
    * - SEField
    * - SELet
    * - SEImplicit
    * - SEQuery
    */
  def transform(lvars: Set[String], expr: SrcExp): SrcExp = {
    expr match {
        case x: SEVar if lvars.contains(x.value) => SELVar(None, x.value)
        case x: SELam => SELam(x.binder, x.t, transform(lvars-x.binder, x.body))
        case x: SEApp => SEApp (transform(lvars, x.e1), transform(lvars,x.e2))
        case x: SELet => SELet(x.lvar, x.scheme, transform(lvars, x.e1), transform (lvars-x.lvar, x.e2))
        case x: SEImplicit => SEImplicit(x.lvars, transform(lvars,x.body))
        case x: SEImpl => SEImpl(x.iface, x.defs.map(t => (t._1,transform(lvars, t._2))), x.t)
        case _ => expr
    }
  }

  def transformLVarToField(fields: Set[String], expr: SrcExp): SrcExp ={
    expr match {
      case x: SEVar if fields.contains(x.value) => SEField(None, x.value)
      case x: SELVar if fields.contains(x.value) => SEField(x.t, x.value)
      case x: SELam => SELam(x.binder, x.t, transformLVarToField(fields-x.binder, x.body))
      case x: SEApp => SEApp (transformLVarToField(fields, x.e1), transformLVarToField(fields,x.e2))
      case x: SELet => SELet(x.lvar, x.scheme, transform(fields, x.e1), transform (fields-x.lvar, x.e2))
      case x: SEImplicit => SEImplicit(x.lvars, transform(fields,x.body))
      case x: SEImpl => SEImpl(x.iface, x.defs.map(t => (t._1,transform(fields, t._2))), x.t)
      case _ => expr
    }

  }

//stub
//  def translate(code: String): String ={
//
//    val v = ExpressionParser.parse(ExpressionParser.program, "interface Eq a = {eq: (a -> a) -> Bool} let a: Int = 5 in 8")
//    println(v)
//    "LALALA"
////    throw new IllegalArgumentException()
//  }

  def main(args: Array[String]): Unit = {
    val program = ExpressionParser.parse(ExpressionParser.program, "interface Eq a = {eq: (a -> a) -> Bool; compare: (a -> a) -> Bool} let a: Int = 5 in 8")
    if (program.successful) {
      val allFields = program.get.interfaces.list.flatMap(interface => interface.declarationList.declarations.map(decl => decl.varName))
      println(allFields)
//      transformVarToLVar(program.get.expr)
//      println(program.get.interfaces.list(0).declarationList)
//      program.get.expr
    }
  }

}
