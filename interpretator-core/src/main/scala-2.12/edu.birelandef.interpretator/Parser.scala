package edu.birelandef.interpretator

import edu.birelandef.interpretator.Token._

import scala.util.parsing.combinator._

object ExpressionParser extends RegexParsers  {

  override val skipWhitespace = true

  /** впомогательные парсеры **/

  def id: Parser[String] = "[a-zA-Z][a-zA-Z0-9]*".r ^^  (_.toString)
  def int: Parser[Int] = "[0-9]+".r ^^  (_.toInt)

/*********************/
  def program: Parser[Program] = interfaces ~ expr ^^ (p=> Program(p._1, p._2))

  // через запятую!!!

  def interface: Parser[Interface] = ("interface" ~> id  ~ varList) ~ ( "=" ~> "{" ~> declList <~ "}") ^^ (p => Interface(p._1._1, p._1._2, p._2))
  val interfaces: Parser[Interfaces] = repsep(interface, "," ) ^^ (Interfaces(_))

  val declList =  repsep(declaration, ";")^^ (DeclList(_))
  def declaration: Parser[Declaration] = (id <~ ":" ) ~ tau ^^ (p => Declaration(p._1, p._2))

  val defList =  repsep(definition, ",") ^^ (DefList(_))
  def definition: Parser[Definition] = (id <~ "=" ) ~ expr ^^ (p => Definition(p._1, p._2))

  def varList: Parser[VarList] =  repsep((id | int ) , ",")  ^^ (p => VarList(p.map(c => c.toString)))


  def tau: Parser[Tau] = ( (atau <~ "->") ~ tau ^^ (p => Tau(p._2.ataus.::(p._1) )))|
          (atau  ^^ (p => Tau(List(p))))

  def atau: Parser[Atau] =  "(" ~> tau <~ ")" ^^ (TauType(_)) | "Int" ^^  (IntegerType(_)) |
    id ~ atauList ^^ (p => distinguishInterfaceVSType(p._1, p._2)) | "(" ~> atauPairList <~ ")" ^^ (PairType(_))

  val atauList = repsep(atau, ",")  ^^ (ATaus(_))

  val atauPairList = repsep(atau, "*")  ^^ (ATaus(_))

  def distinguishInterfaceVSType(id: String, aTaus: ATaus): Atau ={
    if (aTaus.ataus.isEmpty) (TypeVariable(id))
    else  InterfaceType(id, aTaus)
  }

  def expr: Parser[Expr] = implicitExpr |letExpr | lambdaWithTauExpr | lambdaExpr | functionExpr
  def functionExpr: Parser[FunctionExpr] = repsep(axpr, "$") ^^ (FunctionExpr(_))
  def lambdaExpr: Parser[LambdaExpr] = ("\\" ~> vars <~ ".")  ~  expr ^^  (p => (LambdaExpr(p._1, p._2)))
  def lambdaWithTauExpr: Parser[LambdaWithTauExpr] = ("\\" ~> vars <~ ":") ~ (tau <~ ".")  ~  expr ^^  (p => (LambdaWithTauExpr(p._1._1, p._1._2, p._2)))
  def implicitExpr: Parser[ImplicitExpr] = ("implicit" ~> "{" ~> varList <~"}" <~ "in") ~ expr ^^  (p => (ImplicitExpr(p._1, p._2)))
  def letExpr: Parser[LetExpr] = ("let" ~> id) ~ (":" ~> scheme <~ "=") ~ ((expr <~ "in") ~ expr ) ^^  (p=>(LetExpr(p._1._1, p._1._2, p._2._1, p._2._2)))

  val vars = repsep(id, ",")  ^^ (List(_).flatten)

  def axpr:Parser[AExpr] = seInterfaceExt | seInterface | sePair | seExpr | seQuery | seQueryEmpty |  seInt | seVarWithType|seVar
  def seInt: Parser[SEInt] = int ^^ (SEInt(_))
  def seVar: Parser[SEVar] = id ^^ (SEVar(_))
  def seVarWithType: Parser[SEVarWithType] = (id <~":") ~ tau ^^ (p => (SEVarWithType(p._1, p._2)))
  def seQueryEmpty: Parser[SEQueryNothing] = "?" ^^ (SEQueryNothing(_))
  def seQuery: Parser[SEQuery] = "?" ~> tau  ^^ (SEQuery(_))
  def seExpr: Parser[SEExpr] = "(" ~> expr <~ ")" ^^ (SEExpr(_))
  def sePair: Parser[SEPair] = "(" ~> repsep(axpr, "*") <~ ")" ^^ (p => SEPair(p.head,p.tail.head))
  def seInterface: Parser[SEInterface] = (id <~ "{") ~ (defList <~ "}") ^^ (p => SEInterface(p._1, p._2))
  def seInterfaceExt: Parser[SeInterfaceExt] = ((id <~ "{") ~ (defList <~ "}"))  ~ (":"~> id ~ atauList ) ^^
    (p => SeInterfaceExt(p._1._1, p._1._2, p._2._1, p._2._2))



  def scheme:Parser[Scheme] = forallSch | forallVars | schemeSetSch | tauScheme | schemeSet
  def forallSch: Parser[ForallScheme] = ("forall" ~> varList) ~ (("." ~> schemeSet )~ ("=>" ~> tau)) ^^  (p=>(ForallScheme(p._1, p._2._1, p._2._2)))
  def forallVars: Parser[VarsScheme] = ("forall" ~> varList) ~ ("." ~> tau)  ^^  (p=>(VarsScheme(p._1, p._2)))
  def schemeSetSch: Parser[SchemeSetScheme] = (schemeSet <~ "=>")~ tau ^^  (p=>(SchemeSetScheme(p._1, p._2)))
  def tauScheme: Parser[TauScheme] = tau ^^ (TauScheme(_))

  def schemeSet: Parser[SchemeSet] = "{" ~> schemes <~  "}" ^^ (SchemeSet(_))

  def schemes = repsep(scheme, ",")  ^^ (Schemes(_))

}

