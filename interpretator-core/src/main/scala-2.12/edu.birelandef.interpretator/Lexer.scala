package edu.birelandef.interpretator

object Lexer {


  sealed trait Tag {
    def name: String
  }

  case object NONE extends Tag {
    val name = "NONE"
  }

  case object RESERVED extends Tag {
    val name = "RESERVED"
  }

  case object INT extends Tag {
    val name = "INT"
  }

  case object ID extends Tag {
    val name = "ID"
  }

  case class Token(expr: String, tag: Tag)

  val tokens = List(
    Token("0-9", INT),
    Token("[a-zA-Z]", ID),
    Token("\\", RESERVED), //SrcTokenLambda
    Token("implicit", RESERVED),
    Token("interface", RESERVED),
    Token("let", RESERVED),
    Token("in", RESERVED),
    Token("forall", RESERVED),
    Token("Int", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED),
    Token("implicit", RESERVED)
  )
}
