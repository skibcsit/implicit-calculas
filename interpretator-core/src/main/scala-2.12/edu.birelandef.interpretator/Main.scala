package edu.birelandef.interpretator

import scala.io.Source

/**
  * Created by sophie on 23/08/17.
  */
object Main {
  def main(args: Array[String]) = {
    println("Implicit Calculus Interpreter")
    if (args.length < 1)
      println("Error: pls, specify a correct path to source file")
    else
      parseAndEval(args.head)
  }

  def srcparse(code: String): Option[String] ={
    Some ("stub")
  }

  def translateSrcPgm(h: Option[String]): String ={
    ""
  }

  def parseAndEval(filename: String) = {
    println(s"Source file:  $filename")
    val bufferedSource = Source.fromFile(filename)
    val code = bufferedSource.getLines.toList.mkString(" ")
    bufferedSource.close

    // распарсенная на лексемы программа
    val pgm = srcparse(code)
    if (pgm.isDefined) {
      println("#######################################")
      println("#          Source Program             #")
      println("#######################################")
      println(pgm.get)
    } else println("Error: syntax error")
    //распарсенная на IC
    val translatedPgm = translateSrcPgm(pgm)
    println("#######################################")
    println("#          Implicit Calculus          #")
    println("#######################################")
    println(translatedPgm)
  }
}
