package edu.birelandef.interpretator

import edu.birelandef.interpretator.Token.AExpr
import edu.birelandef.interpretator.translator2.Translator.interfacesMap

import scala.collection.mutable.ListBuffer

/**
  * Объекты, на которые разбирается входная строка кода.
  * Created by sophie on 24/08/17.
  */
object Token {

  case class Program(interfaces: Interfaces, expr: Expr)

  case class Interface(name: String, types: VarList, declarationList: DeclList)

  case class Interfaces(list: List[Interface])

//  inner classes

  sealed trait Type

  case class Definition(varName: String, expr: Expr)
  case class DefList(definitions: List[Definition])


  case class Declaration(varName: String, tpe: Tau)
  case class DeclList(declarations: List[Declaration])


  sealed trait Atau{
    def getType(): String
  }
  case class TypeVariable (tpe: String) extends Atau {
    override def getType(): String = tpe
  }
  case class IntegerType(tpe: String)  extends Atau {
    override def getType(): String = tpe
  }
  case class InterfaceType (interfaceName: String, tpe: ATaus) extends Atau {
    override def getType(): String = interfaceName
  }
  case class PairType(tpe: ATaus) extends Atau {
    override def getType(): String = tpe.ataus.map(c => c.getType()).mkString(", ")
  }
  //todo исправить
  case class TauType (tpe: Tau) extends Atau {
    override def getType(): String = tpe.ataus.head.getType()
  }

  case class ATaus(ataus: List[Atau])

  /**
  AExp
    : int                    { SELit $1 }
  | var                    { SEVar $1 }
  | var ':' Tau            { SELVar (Just $3) $1 }
  | '?'                    { SEQuery Nothing }
  | '?' Tau                { SEQuery (Just $2) }
  | '(' Exp ')'            { $2 }
  --  | '(' Exp ':' Scheme ')' { SEAnnot $2 $4 }
  | iface '{' DefList '}'  { SEImpl $1 $3 Nothing }
  | iface '{' DefList '}' ':' iface ATaus { SEImpl $1 $3 (Just (STIface $6 $7)) }
    */

  sealed trait AExpr {
    def getValue(): Any = ""
  }
  case class SEInt(value: Int) extends AExpr {
    override def getValue(): Int = value
  }
  case class SEVar(value: String) extends AExpr{
    override def getValue(): String = value
  }
  case class SEVarWithType(value: String, tpe: Tau) extends AExpr // var ':' Tau
  case class SEQueryNothing(value: String) extends AExpr

  case class SEQuery(value: Tau) extends AExpr
  case class SEExpr(value: Expr) extends AExpr
  case class SEInterface(interfaceName: String, defList: DefList) extends AExpr{
    override def getValue() = (interfaceName, defList.definitions.map(defin => (defin.varName,defin.expr)))
  }
  case class SeInterfaceExt(interfaceName: String, defList: DefList, extInterfaceName: String, aTaus: ATaus) extends AExpr
  case class SEPair(elem1: AExpr, elem2: AExpr) extends AExpr{
    override def getValue(): (Any, Any) = (elem1.getValue(), elem2.getValue())
  }

  /**
  Tau
  : ATau              { $1 }
  | ATau "->" Tau     { STFun $1 $3 }
    */
  case class Tau(ataus: List[Atau])

  case class Schemes(list: List[Scheme]) // ,

  sealed trait Scheme{
    def getRealType(): Any
  }
  //forall VarList '.' SchemeSet "=>" Tau
  case class ForallScheme(vars: VarList, in: SchemeSet, out: Tau) extends Scheme {
    override def getRealType(): Unit = ???
  }
  //forall VarList '.' Tau
  case class VarsScheme(vars: VarList,  tpe: Tau) extends Scheme {
    override def getRealType(): Unit = ???
  }
  //SchemeSet "=>" Tau
  case class SchemeSetScheme(in: SchemeSet, out: Tau) extends Scheme {
    override def getRealType(): Unit = ???
  }
  case class TauScheme(value: Tau) extends Scheme {
    override def getRealType(): List[(Any, Any)] = {
          var temp = value.ataus.map (x =>
            x match {
            case x: TypeVariable => if (x.tpe.equals ("Bool") ) (Boolean, x) else (Char, x)
            case x: IntegerType => (Int, x)
            case x: PairType => (Tuple2, (x.tpe.ataus.map (t => t.getType() ) ))
            case x: InterfaceType => (Interface, x)
            case _=> ("","")
          }
        )
//      ("","")
      temp
    }

  } //Tau
  case class SchemeSet(schemes: Schemes) extends Scheme {
    override def getRealType(): Unit = ???
  } // { }

  case class VarList(vars: List[String])

  sealed trait Expr
  case class FunctionExpr (inType: List[AExpr]) extends Expr
//  case class AExprList(expr: List[AExpr]) extends Expr
  //| '\\' var '.' Exp
  case class LambdaExpr(varNames: List[String], expr: Expr) extends Expr
  //'\\' var ':' Tau '.' Exp
  case class LambdaWithTauExpr(varNames: List[String], varType: Tau, expr: Expr) extends Expr
  //implicit '{' VarList '}' in Exp
  case class ImplicitExpr(vars: VarList, expr: Expr) extends Expr
  //let var ':' Schemes '=' Exp in Exp
  case class LetExpr(varName: String, scheme: Scheme, replaceExpr: Expr, expr: Expr) extends Expr


}


