//package edu.birelandef.interpretator
//
//import java.io
//
//import edu.birelandef.interpretator.types.PTypes._
//import edu.burelandef.interpretator.types.FTypes._
//
//import scala.collection.immutable
//
//object FTranslator {
//
//  /**
//    * Главный метод по переводу из P системы в F систему
//    * @param pExpr
//    * @return
//    */
//  def translate(pExpr:PExp):Option[FExp] = ???
//
//  def translatePExp(env: Map[PContext, Int], pExp: PExp, n: Int): Option[(FExp, Int)] =
//    pExp match {
//      case x:EVar => Some(FEVar(x.e),n)
//      case x:ELit => Some(FELit(x.e),n)
//      case x:ELam => { val tmp = translatePExp(env, x.f(n), n + 1)
//        val newFExpr = FELam(translatePContext(x.c), x => renameVarInFExp(tmp.get._1, n, x))
//        Option(newFExpr, tmp.get._2)
//      }
//      case x:EApp => {
//                      val tmpA = translatePExp(env, x.e1,n)
//                      val tmpB = translatePExp(env, x.e2,tmpA.get._2)
//                      Some(FEApp(tmpA.get._1, tmpB.get._1), tmpB.get._2 )
//
//      }
//      case x: ETLam => {
//                        val tmp = translatePExp(env, x.function.apply(n), n + 1)
//                       Some(FETLam (g => renameTVarInFExp(tmp.get._1, n, g)), tmp.get._2)
//      }
//      case x: ETApp => {
//                        val tmpA = translatePExp(env, x.expr, n)
//                        val typeB = translatePContext(x.context)
//                        Some(FETApp(tmpA.get._1, typeB), tmpA.get._2)
//      }
//      case x: EILam => {
//                        val t = translatePContext(x.context)
//                        val tmp = translatePExp (env+(x.context-> n), x.expr, n + 1)
//                      Some(FELam(t, g => renameVarInFExp(tmp.get._1, n,g)), tmp.get._2)
//      }
//      case x: EIApp =>  {
//                      val tmpA =translatePExp(env, x.e1, n)
//                      val tmpB = translatePExp(env, x.e2, tmpA.get._2)
//                      Some(FEApp(tmpA.get._1, tmpB.get._1) , tmpB.get._2)
//      }
//      case x: EQuery => translateQuery(env,x.context, n)
//      case x: ERecord => {
//        val (fields1, n1): (List[(String, FExp)], Int) = x.fields.foldLeft(List.empty, 0)
//        (fs: List[(String, PExp)], m: Int, u: String, v: PExp) => {
////          val (v1, innerN) = translatePExp(env, v, m)
////          val p: (String, FExp) = (u, v1.get);
////          val t: (List[(String, FExp)], Int) = (fs.::(p), innerN)
////          t
//          (List.empty.::(("", FEVar(5))), 7)
//        }
//
//        Some(FERecord(x.name, fields1), n1 )
//      }
//      case x: EField => Some(FEField(x.name), n)
//
//  }
//
//  def translateQuery(env: List[(PContext, Int)], pContext: PContext,n: Int): Option[(FExp ,Int)] =
//    pContext match {
//      case x: Forall => val (e, n1) = translateQuery(env, x.function.apply(n), n + 1).get; Some(FETLam(g =>renameTVarInFExp(e, n, g)), n1)
//      case x: Rule => val (e, n1) = translateQuery(env.::((x.in, n)), x.out, n).get; Some(FELam(translatePContext(x.in), g => renameVarInFExp(e, n, g)),n1)
//      case x: Type => {
//        val (rs, ws, e, n1) = matchesFirst(env, x.tpe, n).get
//        val (rs1, n2) = translateAll(env, rs, n1).get
//        Some(substFExp(ws.zip(rs1), e), n2)
//      }
//
//    }
//
//  def matchesFirst(env: List[(PContext, Int)], pType: PType, n: Int): Option[(List[PContext], List[Int], FExp, Int)] = ???
//
//  def matches(pContext: PContext, pType: PType, x:Int, n: Int): Option[(PContext,Int,FExp, Int)] =
//    go(pContext,pType, n, List.empty[PContext], List.empty[Int], List.empty[Int],  FEVar(x))
//
//
//  def go (pContext: PContext,  pType: PType,n: Int, recs: List[PContext], vars: List[Int], evs: List[Int], exp: FExp): Option[(PContext,Int,FExp, Int)] ={
//    pContext match {
//      case c:Type => {
//        val subst = unify(c.tpe, pType,  n, vars).get
//        val subst1: List[(Int, FType)] = subst.map(v => (v._1, translatePContext(v._2)))
//          (recs.map(apply subst1) , evs, substTyFExp(subst1, exp), n)
//      }
//      case c:Forall => go(c.function(n), pType,n + 1, recs, vars.::(n), evs, FETApp(exp,FTVar(n)))
//      case c:Rule => go(c.out, pType, n + 1, recs.::(c.in), vars, evs.::(n), FEApp(exp,FEVar(n)))
//    }
//  }
//
//
//  def apply(subst: List [(Int,PContext)], r: PContext): PContext = gor(subst, r)
//
//  def gor(subst: List [(Int,PContext)], r: PContext): PContext =
//    r match {
//      case r: Type => got(subst,r.tpe)
////      case r: Forall => Forall (gor.r.function)
//      case r: Rule =>Rule (gor(subst,r.in) ,gor(subst, r.out))
//    }
//
//  def got(subst: List [(Int,PContext)], t: PType):Type =
//    t match {
//      case x: Var => val tmp =subst.toMap.get(x.a)
//                     tmp match {
//                       case Some(Type(n)) => Type(n)
//                       case None => Type(Var(x.a))
//                     }
//      case x: TInt => Type(x)
//      case x: Fun => Type(Fun(gor(subst,x.in), gor(subst,x.out)))
//      case x: Record => Type(Record (x.name, x.types.map(c => got(subst, c).tpe )))
//    }
//
//
//  def unify( t1: PType,  t2: PType, n: Int, vars: List[Int]): Option[List[(Int,PContext)]]{
//    // из execStateT, который возвращает StateT, и извлекаем из монады
//    //    > unify t1 t2 n vars = execStateT (got t1 t2) (n,[]) >>= return . snd where
//  }
//
//  def got4Unify(t1: PType, t2: PType,  n: Int, vars: List[Int]): Unit =
//    t1 match {
//      case x:Var if vars.contains(x.a) => ???
//      case _ => got_4Unify(t1, t2, n, vars)
//    }
//
//  def got_4Unify(r1: PType, r2: PType ,  n: Int, vars: List[Int]): Unit ={
//
//  }
//
//  def gor4Unify(r1: PContext, r2: PContext,  n: Int, vars: List[Int]): Unit ={
//
//  }
//
//  def gor_4Unify(r1: PContext, r2: PContext ,  n: Int, vars: List[Int]): Unit ={
//    (r1, r2) match {
//      case (x: Type, y: Type) => got4Unify(x.tpe, y.tpe, n, vars )
//      case (x: Forall, y: Forall) => got4Unify(x.tpe, y.tpe, n, vars )
//      case (x: Rule, y: Rule) => gor4Unify(x.in, y.in, n, vars) ; gor4Unify(x.out, y.out, n, vars)
//      case _ => throw new IllegalArgumentException("gor_4Unify")
//    }
//  }
//
//
//
//
//  def translateAll(env: List[(PContext, Int)], pContexts: List[PContext], n: Int ) : Option[(List[FExp], Int)] =
//    if (pContexts.nonEmpty){
//      val (r1, n1) = translateQuery(env, pContexts.head, n).get
//      val (rs1, n2) = translateAll(env, pContexts.tail, n1).get
//       Some(r1::rs1, n2)
//    } else
//      Some(List.empty[FExp], n)
//
//  def substFExp(list: List[(Int, FExp)], inExp: FExp): FExp ={
//    list.foldLeft(inExp) (substOneFExp)
//  }
//
//  def substOneFExp(fExp: FExp, tuple: (Int, FExp)): FExp =
//    fExp match {
//      case x: FEVar if x.value==tuple._1 => tuple._2
//      case x: FELam  =>  FELam(x.tpe, g => substOneFExp(x.function.apply(g), tuple))
//      case x: FEApp  =>  FEApp(substOneFExp(x.exp1, tuple), substOneFExp(x.exp2,tuple))
//      case x: FETLam => FETLam(g=> substOneFExp(x.function.apply(g), tuple))
//      case x: FETApp => FETApp(substOneFExp(x.exp, tuple), x.tpe)
//      case x: FERecord  =>  FERecord(x.name, x.fields.map(field => (field._1, substOneFExp(field._2, tuple))))
//      case _ => _
//    }
//
//  def substTyFExp(list: List[(Int, FType)], fExp: FExp):FExp = list.foldLeft(fExp) (substTyOneFExp)
//
//  def substTyOneFExp (fExp: FExp, fTyteRec: (Int, FType)): FExp =
//    fExp match {
//      case x: FELam => FELam(substTyOneFType(x.tpe,fTyteRec) , g=> substTyOneFExp(x.function.apply(g), fTyteRec))
//      case x: FEApp => FEApp(substTyOneFExp(x.exp1,fTyteRec), substTyOneFExp(x.exp2, fTyteRec))
//      case x: FETLam => FETLam (g => substTyOneFExp(x.function.apply(g), fTyteRec))
//      case x: FETApp => FETApp(substTyOneFExp(x.exp, fTyteRec), substTyOneFType(x.tpe, fTyteRec))
//      case x: FERecord => FERecord(x.name, x.fields.map(t =>  (t._1, substTyOneFExp(t._2, fTyteRec))))
//      case _ =>  _
//    }
//
//  def substTyOneFType(fType: FType, tuple: (Int, FType)) :FType =
//    fType match {
//      case x: FTVar if x.a == tuple._1 => tuple._2
//      case x: FTFun => FTFun(substTyOneFType(x.inType, tuple),  substTyOneFType(x.outType, tuple))
//      case x: FTForall =>  FTForall (g =>  substTyOneFType(x.function.apply(g), tuple))
//      case x: FTRecord => FTRecord ( x.name, x.types.map (t => substTyOneFType(t, tuple)))
//      case _ => _
//    }
//  /**
//    * Извлекает из P контекстов типы для F системы
//    * Контекст в P системе - расширенная версия типов в F системе
//    * @param context PContext
//    * @return FType
//    */
//  def translatePContext(context: PContext): FType =
//    context match {
//      case d: Type => translatePType(d.tpe)
//      case d: Forall => FTForall(g => translatePContext(d.function.apply(g)))
//      case d: Rule => FTFun (translatePContext(d.in), translatePContext(d.out))
//    }
//
//  def translatePType(pType: PType): FType =
//    pType match {
//      case d: Var => FTVar(d.a)
//      case d: TInt => FTInt()
//      case d: Fun => FTFun (translatePContext(d.in), translatePContext(d.out))
//      case d: Record => FTRecord(d.name, d.types.map(ty => translatePType(ty)))
//
//    }
//
//  def renameVarInFExp(fExp: FExp, a:Int, b:Int): FExp =
//    fExp match  {
//      case d:FEVar if d.value==a => FEVar(b)
//      case d:FELam => FELam(d.tpe, g =>renameVarInFExp(d.function.apply((g)),a,b))
//      case d:FEApp => FEApp (renameVarInFExp(d.exp1,a,b),renameVarInFExp(d.exp2, a, b))
//      case d:FETLam => FETLam(g =>renameVarInFExp(d.function.apply(g),a,b))
//      case d:FETApp => FETApp (renameVarInFExp(d.exp,a,b), d.tpe)
//      case d:FERecord => FERecord(d.name,d.fields.map(field => (field._1, renameVarInFExp(field._2,a,b))))
//      case _ => _
//    }
//
//  def renameTVarInFExp(fExp: FExp, a:Int, b:Int): FExp =
//    fExp match {
//      case d:FELam => FELam(renameTVarInFType(d.tpe, a, b), g =>renameTVarInFExp(d.function.apply((g)),a,b))
//      case d:FEApp => FEApp (renameTVarInFExp(d.exp1,a,b), renameTVarInFExp(d.exp2, a,b))
//      case d:FETLam => FETLam (g =>renameTVarInFExp (d.function(g), a, b))
//      case d:FETApp => FETApp (renameTVarInFExp(d.exp,a,b), renameTVarInFType(d.tpe,a,b))
//      case d:FERecord =>FERecord(d.name,d.fields.map(field => (field._1, renameTVarInFExp(field._2,a,b))))
//      case _ => _
//    }
//
//  def renameTVarInFType(ftype:FType, a:Int, b:Int): FType =
//    ftype match {
//      case d:FTVar if d.a==a => FTVar(b)
//      case d:FTFun => FTFun(renameTVarInFType(d.inType,a,b),renameTVarInFType(d.outType,a,b))
//      case d:FTForall => FTForall(g => renameTVarInFType(d.function.apply(g),a,b))
//      case d:FTRecord => FTRecord(d.name, d.types.map(ty => renameTVarInFType(ty, a,b)))
//      case _ => _
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
