package edu.birelandef.interpretator.translator2

object ProbaObject {
  def main(args: Array[String]): Unit = {

    def add (firstArg: Int): Boolean = ((firstArg % 2) == 0)
    def view (firstArg: Int): Boolean = ((firstArg % 2) == 0)
    def remove(firstArg: Int): Boolean = ((firstArg % 2) == 0)

    def isEven(firstArg: Int): Boolean = ((firstArg % 2) == 0)


    val dispatch2: Map[String, (Int) => Any] = Map(
      "add" -> isEven,
      "view" -> view,
      "remove" -> remove
    )
    print(dispatch2.get("add").get.apply(4))



//    println(cachedFun(4,8))
  }

}
