package edu.birelandef.interpretator.translator2

object ServiceFunctionUtils {
  def isEven(firstArg: Int): Boolean = {
    ((firstArg % 2) == 0)
  }

  def primEqInt (firstArg: Int, secondArg: Int): Boolean ={
    firstArg.equals(secondArg)
  }

}
