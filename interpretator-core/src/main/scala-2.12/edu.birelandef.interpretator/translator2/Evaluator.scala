package edu.birelandef.interpretator.translator2

import edu.birelandef.interpretator.Token._
import edu.birelandef.interpretator.translator2.Translator.{implicitQueue}

object Evaluator {

  var currentFunction2: (Int, Int) => Any  = null
  var currentFunction1: (Int) => Any  = null

    def eval(expr: Expr): Any ={
      val result = expr match {
        case x: FunctionExpr => x.inType.map(innerEval)
  //      case x: AExprList => eval(x.expr.head)
        case x: ImplicitExpr => eval(x.expr)
        case x: LetExpr => eval(x.expr)
        case _ => None
      }
      Translator.clearAllMaps()
      result
  }
  def innerEval(expr: AExpr): Any ={
    expr match {
      case x: SEInt =>  {
        applyFunction(x.value)
        }
      case x: SEVar => {
        if (Translator.explicitValueMap.contains(x.value)) {
          val res = Translator.explicitValueMap.get(x.value).get
          if (res.isInstanceOf[Int])
            return applyFunction(res.asInstanceOf[Int])
          else return res
        }

        if (Translator.explicitFunction1Map.contains(x.value)) {
          currentFunction1 = Translator.explicitFunction1Map.get(x.value).get
        }
        if (Translator.explicitFunction2Map.contains(x.value)) {
          currentFunction2 = Translator.explicitFunction2Map.get(x.value).get
        }
      }
      case x: SEExpr => eval(x.value)
      case x: SEQuery => {
        val temp = x.value.ataus;
        println("temp " + temp)
        println("implicitQueue " + implicitQueue)
        val res = temp.map(c => {
          println("Lookup Type ", c.getType()); lookupInImplicitQueue(implicitQueue.iterator, c.getType())
        })
        res.foreach( res1 =>
        if (res1.isInstanceOf[Int])
          return applyFunction(res1.asInstanceOf[Int])
//        else return res
        )
        res
      }
      case _ => ""
    }
  }


  def flatten(ls: List[Any]): List[Any] = ls flatMap {
    case ms: List[_] => flatten(ms)
    case e => List(e)
  }

  def applyFunction(value: Int): Any ={
    if (currentFunction1!= null)  {
      val resulr = currentFunction1.apply(value)
      currentFunction1 = null
      resulr
    } else if (currentFunction2!= null) {
      currentFunction1 = currentFunction2.curried.apply(value)
      currentFunction2 = null
    } else  value
  }

  def lookupInImplicitQueue(iterator: Iterator[Translator.ImplicitContext], tpe: String): Any ={
    var currentContext = iterator.next()
    if (currentContext.rightType.equalsIgnoreCase(tpe)){
      if (currentContext.leftType.isEmpty)
        currentContext.value
      else
        lookupInImplicitQueue(iterator, currentContext.leftType)
    }
    else
      lookupInImplicitQueue(iterator, tpe)

  }
}
