package edu.birelandef.interpretator.translator2

import edu.birelandef.interpretator.ExpressionParser
import edu.birelandef.interpretator.Token._

import scala.collection.mutable

/**
  * Транслятор из исходников в систему P
  */
object Translator {



  case class ImplicitContext (leftType: String, rightType: String, value: Any)
  case class ExplicitContext (name: String, scheme: Scheme)

  var implicitQueue =  new scala.collection.mutable.Stack[ImplicitContext]
  var interfacesMap = new mutable.HashMap[String, Interface]


  var explicitFunction2Map: Map[String, (Int, Int) => Any] = Map(
    "primEqInt" -> ServiceFunctionUtils.primEqInt
  )

  var explicitFunction1Map: Map[String, (Int) => Any] = Map(
    "isEven" -> ServiceFunctionUtils.isEven
  )

  var explicitValueMap: Map[String, Any] = Map()


//  explicitMap.head.action.apply()
//  explicitMap.+(explValue)

//stub
  def translate(code: String): ExpressionParser.ParseResult[Program] ={
    val v: ExpressionParser.ParseResult[Program] = ExpressionParser.parse(ExpressionParser.program, code)
//    "interface Eq a = {eq: (a -> a) -> Bool} let a: Int = 5 in 8"
    println("##########################"*5)
    println(code)
    println("##########################"*5)
    println("Translation Result")
    println(v)
    v.get.interfaces.list.map(i => interfacesMap+=(i.name -> i))
    extractToImplicitEnviroment(v.get.expr)
    extractToExplicitEnviroment(v.get.expr)
    v
  }

  def main(args: Array[String]): Unit = {

//    val value = ExpressionParser.parse(ExpressionParser.interfaces, "interface Eq a = { eq : ( a -> a -> Bool ) }")
    val value = ExpressionParser.parse(ExpressionParser.expr, "implicit {56} in implicit {42} in let p1: (Int*Bool) = (4*True) in let p2: Int = 42 in primEqInt $ p2 $ ?Int")
//    val value = ExpressionParser.parse(ExpressionParser.letExpr, "let eqInt1: Eq Int = Eq {eq = primEqInt} in let p2: Int = 4 in p1")
        println(value)
        println("##########################"*5)
        extractToImplicitEnviroment(value.get)
        extractToExplicitEnviroment(value.get)

        println("explicitValueMap " + explicitValueMap)
        println("##########################"*5)
//        println(explicitValueMap.get("p1").get)
        println("Eval result " + Evaluator.eval(value.get))
//    val value = ExpressionParser.parse(ExpressionParser.letExpr, "let p: (Int*Bool) = (4*True) in 3")
//    println(value)
//    println("##########################"*5)
//    val value1 = ExpressionParser.parse(ExpressionParser.letExpr, "let p: Int = 4 in 3")
//    println(value1)
//    extractToExplicitEnviroment(value1.get)
//    println(explicitValueMap.size)
////    val value = ExpressionParser.parse(ExpressionParser.lambdaExpr, "\\x, y. isEven x")
////    println(value)
////    println("##########################"*5)
//    println(explicitIntMap.size)
//
//    val value2 = ExpressionParser.parse(ExpressionParser.seInterface, "Eq {eq = \\ x. isEven}")
//    println(value2)
//    println("##########################"*5)
//
//    val value3 = ExpressionParser.parse(ExpressionParser.letExpr, "let eqInt2: Eq Int = Eq {eq = \\ x y. isEven x} in 3")
//    println(value3)
//    println("##########################"*5)

//    val program = ExpressionParser.parse(ExpressionParser.program, "implicit {True} in implicit {42} in implicit {54} in (?Int_?Bool)")
//    if (program.successful) {
//      println(program.get)
//      extractToImplicitEnviroment(program.get.expr)
//      println(implicitQueue)
//      println(s"Eval result:" , eval(program.get.expr))
//
//    }
  }

  def extractToImplicitEnviroment(expr: Expr): Unit = {
    expr match {
      case x: ImplicitExpr => {x.vars.vars.map(insertToImplicitQueue); extractToImplicitEnviroment(x.expr)}
      case _ => return
    }
  }

  def extractToExplicitEnviroment(expr: Expr): Unit = {
    expr match {
      case x: ImplicitExpr => extractToExplicitEnviroment(x.expr)
      case x: LetExpr => {
        x.scheme match {
            //простой тип или пара
          case y: TauScheme => {
            println("Add to explicit map  a value of " + x.scheme.getRealType() + " as " + x.varName)
            println("Replace expr  " + x.replaceExpr)
            if (x.replaceExpr.isInstanceOf[FunctionExpr]){
              val newValue = (x.replaceExpr).asInstanceOf[FunctionExpr].inType.head.getValue()
              explicitValueMap+=(x.varName -> newValue)
              println("Added value " + newValue)
            }
          }
//          case y:
        }
        extractToExplicitEnviroment(x.expr)
      }
      case _ => return
    }
  }

  def clearAllMaps(): Unit ={
    implicitQueue.clear()
    Translator.explicitValueMap = Map()
    explicitFunction2Map = Map(
      "primEqInt" -> ServiceFunctionUtils.primEqInt
    )
    explicitFunction1Map = Map(
      "isEven" -> ServiceFunctionUtils.isEven
    )
  }


  def insertToImplicitQueue(implicitValue: String): Unit ={
    if (implicitValue.equalsIgnoreCase("True") || implicitValue.equalsIgnoreCase("False"))
      implicitQueue.push(ImplicitContext("","Bool", implicitValue.equalsIgnoreCase("True")))
    if (implicitValue)
      implicitQueue.push(ImplicitContext("", "Int", implicitValue.toInt))
  }

  implicit def isNumeric(input: String): Boolean = input.forall(_.isDigit)
}
