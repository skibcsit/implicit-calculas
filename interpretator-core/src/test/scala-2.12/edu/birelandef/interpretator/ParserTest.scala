package edu.birelandef.interpretator

import edu.birelandef.interpretator.Token.{Atau, _}
import org.scalatest.{BeforeAndAfter, FunSuite}

  class ParserTest  extends FunSuite with BeforeAndAfter  {

    val letExpr = LetExpr("a",TauScheme(Tau(List(IntegerType("Int")))),FunctionExpr(List(SEInt(5))),FunctionExpr(List(SEInt(8))))

    val interface = Interface("Eq", VarList(List("a")),DeclList(List(Declaration("eq", Tau(List(TauType(Tau(List(TypeVariable("a"),TypeVariable("a")))), TypeVariable("Bool")))))))

    val tauScheme = TauScheme(Tau(List(IntegerType("Int"))))

    //
    //  test("parse simple program"){
    //    ExpressionParser.parse(ExpressionParser.program,
    //      //      "let succIntImpl : Succ Int = Succ { succ = \\x : Int. x } : Succ Int in T" //+
    //      //      "Succ { succ = \\x : Int. x } : Succ Int" //+
    //      //        "implicit { succIntImpl } in (? (Succ Int))"
    //      "interface Succ a = { succ : ( a -> a ) }" +
    //        "let succIntImpl : Succ Int = Succ { succ = \\x : Int. x } : Succ Int in" +
    //        "implicit { succIntImpl } in (? (Succ Int))"
    //      //        "? (Succ Int)"
    //    )
    //  }

//    test("parse tau"){
//      assertResult(Tau(List(IntegerType("Int")))) {
//        ExpressionParser.parse(ExpressionParser.tau, "Int").get
//      }
//      assertResult(Tau(List(IntegerType("Int"),IntegerType("Int")))) {
//        ExpressionParser.parse(ExpressionParser.tau, "Int -> Int").get
//      }
//      assertResult(Tau(List(TauType(Tau(List(IntegerType("Int"), IntegerType("Int")))), IntegerType("Int"))) ) {
//        ExpressionParser.parse(ExpressionParser.tau, "(Int -> Int) -> Int").get
//      }
//    }
//
    test("parse atau"){
      assertResult(IntegerType("Int")) {
        ExpressionParser.parse(ExpressionParser.atau, "Int").get
      }
      assertResult(TypeVariable("T")) {
        ExpressionParser.parse(ExpressionParser.atau, "T").get
      }
      assertResult(TauType(Tau(List(TypeVariable("T"), IntegerType("Int"))))) {
        ExpressionParser.parse(ExpressionParser.atau, "(T -> Int)").get
      }
      assertResult(InterfaceType("ifaceName", ATaus(List(IntegerType("Int"), TypeVariable("Bool"))))){
        ExpressionParser.parse(ExpressionParser.atau, "ifaceName Int,Bool").get
      }

      assertResult(PairType(ATaus(List(IntegerType("Int"), TypeVariable("Bool"))))) {
        ExpressionParser.parse(ExpressionParser.atau, "(Int*Bool)").get
      }

      assertResult(InterfaceType("ifaceName",ATaus(List(IntegerType("Int"), TypeVariable("Bool"),
        PairType(ATaus(List(IntegerType("Int"), TypeVariable("Bool")))))))){
        ExpressionParser.parse(ExpressionParser.atau, "ifaceName Int,Bool,(Int*Bool)").get
      }

    }

    test("parse atauList"){
      assertResult(ATaus(List(IntegerType("Int"),IntegerType("Int"),IntegerType("Int")))) {
        ExpressionParser.parse(ExpressionParser.atauList, "Int,Int,Int").get
      }
    }
//
//
//    //готово (но функции через _)
//    test("parse expr"){
//      assertResult(letExpr)
//        {ExpressionParser.parse(ExpressionParser.expr, "let a: Int = 5 in 8").get
//      }
//
//      assertResult(ImplicitExpr(VarList(List("x","y","z")),letExpr)){
//        ExpressionParser.parse(ExpressionParser.expr, "implicit {x,y,z} in let a: Int = 5 in 8").get
//      }
//
//      assertResult(LambdaWithTauExpr("x",Tau(List(TypeVariable("A"))),letExpr)) {
//        ExpressionParser.parse(ExpressionParser.expr, "\\ x: A. let a: Int = 5 in 8").get
//      }
//
//      assertResult(LambdaExpr("x",letExpr)){
//        ExpressionParser.parse(ExpressionParser.expr, "\\ x. let a: Int = 5 in 8").get
//      }
//
//      assertResult(FunctionExpr(List(SEInt(4), SEVar("settings")))) {
//        ExpressionParser.parse(ExpressionParser.expr, "4_settings").get
//      }
//
//    }
//    //готово
//    test("parse axpr"){
//      assertResult(SEInt(5)){
//        ExpressionParser.parse(ExpressionParser.axpr,"5").get
//      }
//      assertResult(SEVar("f")){
//        ExpressionParser.parse(ExpressionParser.axpr,"f").get
//      }
//      assertResult(SEVarWithType("f", Tau(List(TypeVariable("T"),TypeVariable("P"))))) {
//        ExpressionParser.parse(ExpressionParser.axpr,"f: T -> P").get
//        }
//
//      assertResult(SEQueryNothing("?")){
//        ExpressionParser.parse(ExpressionParser.axpr,"?").get
//      }
//
//      assertResult(SEQuery(Tau(List(TauType(Tau(List(IntegerType("Int"), TypeVariable("A")))), TypeVariable("B"))))) {
//        ExpressionParser.parse(ExpressionParser.axpr,"? (Int -> A) -> B").get
//      }
//
//      assertResult(SEExpr(LetExpr("a",TauScheme(Tau(List(IntegerType("Int")))),FunctionExpr(List(SEInt(5))),FunctionExpr(List(SEInt(8)))))) {
//        ExpressionParser.parse(ExpressionParser.axpr,"(let a: Int = 5 in 8)").get
//      }
//
//      assertResult(SEInterface("Eq", DefList(List(Definition("eq",FunctionExpr(List(SEVar("primEqInt")))))))) {
//        ExpressionParser.parse(ExpressionParser.axpr,"Eq {eq = primEqInt }").get
//      }
//
//      assertResult(SeInterfaceExt("Eq", DefList(List(Definition("eq",FunctionExpr(List(SEVar("primEqInt")))))),
//              "T", ATaus(List(TypeVariable("Y"))))) {
//        ExpressionParser.parse(ExpressionParser.axpr,"Eq {eq = primEqInt }: T Y").get
//      }
//    }
//
//    test("parse var list"){
//      assertResult(VarList(List("x","y","z"))){
//        ExpressionParser.parse(ExpressionParser.varList, "x, y,z").get
//      }
//    }
//
//    test("parse def list"){
//      assertResult(DefList(List(Definition("x",FunctionExpr(List(SEInt(5)))),Definition("y",FunctionExpr(List(SEVar("z"))))))) {
//        ExpressionParser.parse(ExpressionParser.defList, "x = 5, y = z").get
//      }
//    }
//
//    test("parse decl list"){
//      assertResult(DeclList(List(Declaration("x",Tau(List(IntegerType("Int")))),Declaration("y",Tau(List(TypeVariable("String"))))))) {
//        ExpressionParser.parse(ExpressionParser.declList, "x : Int ; y : String").get
//      }
//    }
//
//    test("parse interface"){
//      assertResult(interface){
//        ExpressionParser.parse(ExpressionParser.interface, "interface Eq a = {eq: (a -> a) -> Bool}").get
//      }
//    }
//
//    test("parse interfaces"){
//      assertResult(Interfaces(List(interface, interface))){
//        ExpressionParser.parse(ExpressionParser.interfaces, "interface Eq a = {eq: (a -> a) -> Bool}, interface Eq a = {eq: (a -> a) -> Bool}").get
//      }
//    }
//
//    test("parse scheme"){
//      assertResult(tauScheme){
//        ExpressionParser.parse(ExpressionParser.scheme, "Int").get
//      }
//      assertResult(ForallScheme(VarList(List("a","b")), SchemeSet(Schemes(List(TauScheme(Tau(List(TypeVariable("a"))))))),Tau(List(TypeVariable("T"))))){
//        ExpressionParser.parse(ExpressionParser.scheme, "forall a,b. {a} => T").get
//      }
//
//      assertResult(VarsScheme(VarList(List("a","b")),Tau(List(TypeVariable("T"))))){
//        ExpressionParser.parse(ExpressionParser.scheme, "forall a,b. T").get
//      }
//
//      assertResult(SchemeSetScheme(SchemeSet(Schemes(List(tauScheme,tauScheme))),Tau(List(TypeVariable("T"))))){
//        ExpressionParser.parse(ExpressionParser.scheme, "{Int, Int} => T").get
//      }
//    }
//
//
//    test("parse schemes"){
//      assertResult(Schemes(List(tauScheme,tauScheme))){
//        ExpressionParser.parse(ExpressionParser.schemes, "Int, Int").get
//      }
//    }
//
//    test("parse scheme set"){
//      assertResult(SchemeSet(Schemes(List(tauScheme,tauScheme)))){
//        ExpressionParser.parse(ExpressionParser.schemeSet, "{Int , Int}").get
//      }
//    }
//
//    test("PARSE PROGRAM"){
//      assertResult(Program(Interfaces(List(interface)), letExpr)){
//        ExpressionParser.parse(ExpressionParser.program, "interface Eq a = {eq: (a -> a) -> Bool} let a: Int = 5 in 8").get
//      }
//    }

    test("PARSE EXECUTED PROGRAM"){
      assertResult(ImplicitExpr(VarList(List("1")),ImplicitExpr(VarList(List("True")),FunctionExpr(List(SEQuery(Tau(List(IntegerType("Int"))))))))){
        ExpressionParser.parse(ExpressionParser.implicitExpr, "implicit {1} in implicit {True} in ?Int").get
      }
      assertResult(ImplicitExpr(VarList(List("1")),ImplicitExpr(VarList(List("y")),FunctionExpr(List((SEInt(8))))))){
        ExpressionParser.parse(ExpressionParser.implicitExpr, "implicit {1} in implicit {y} in 8").get
      }
    }

    test("parse var list"){
      assertResult(VarList(List("x","1","y"))){
        ExpressionParser.parse(ExpressionParser.varList, "x, 1, y").get
      }
    }
  }