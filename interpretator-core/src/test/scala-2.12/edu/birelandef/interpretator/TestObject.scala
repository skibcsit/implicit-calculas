package edu.birelandef.interpretator

import scala.util.parsing.combinator.{PackratParsers, RegexParsers}

/**
  * Created by sophie on 28/08/17.
  */
object TestObject {
}


class SimpleParser extends RegexParsers {
  def word: Parser[String]   = """[a-z]+""".r       ^^ { _.toString }
  def number: Parser[Int]    = """(0|[1-9]\d*)""".r ^^ { _.toInt }
  def freq: Parser[WordFreq] = word ~ number        ^^ { case wd ~ fr => WordFreq(wd,fr) }
}

case class WordFreq(word: String, count: Int) {
  override def toString = "Word <" + word + "> " +
    "occurs with frequency " + count
}
//
//object TestSimpleParser extends ExpressionParser {
//  def main(letExpr: Array[String]) = {
//    parse(atau,"Int"
//      //      "let succIntImpl : Succ Int = Succ { succ = \\x : Int. x } : Succ Int in T" //+
//      //      "Succ { succ = \\x : Int. x } : Succ Int" //+
//      //        "implicit { succIntImpl } in (? (Succ Int))"
////      "interface Succ a = { succ : ( a -> a ) }" +
////        "let succIntImpl : Succ Int = Succ { succ = \\x : Int. x } : Succ Int in" +
////        "implicit { succIntImpl } in (? (Succ Int))"
//      //        "? (Succ Int)"
//    )
//    match {
//      case Success(matched,_) => println(matched)
//      case Failure(msg,_) => println("FAILURE: " + msg)
//      case Error(msg,_) => println("ERROR: " + msg)
//    }
//  }
//}

