package edu.birelandef.interpretator

import edu.birelandef.interpretator.types.PTypes.TInt
import edu.birelandef.interpretator.types.STypes.{STFun, STIface, STInt, STVar}
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter

class TranslatorTest  extends FunSuite with BeforeAndAfter {
  var srcVal: STVar = _
  var srcInt: STInt = _
  var srcFun: STFun = _
  var srcIface: STIface = _

  before {
    //Src Type
    srcVal = STVar("T")
    srcInt = STInt()
    srcFun = STFun(srcVal,srcInt)
    srcIface = STIface("", List(srcVal))

  }

//  test("translateSrcType for all types") {
//    assert(translator.translateSrcType(srcInt, 1, Map("" -> 6)).equals(TInt()))
//  }
}
